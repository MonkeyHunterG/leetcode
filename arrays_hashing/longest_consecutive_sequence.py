def longestConsecutiveSequence(nums: list[int]) -> int:
    if len(nums) == 0:
        return 0
    counter = 1
    my_set = set(nums)
    arr = []

    for num in my_set:
        if num - 1 not in my_set:
            arr.append(num)
    
    for a in arr:
        local_counter = 1
        while a + 1 in my_set:
            local_counter += 1
            a += 1
        if local_counter > counter:
            counter = local_counter

    return counter

i1 = [100,4,200,1,3,2]
o1 = 4 
i2 = [0,3,7,2,5,8,4,6,0,1]
o2 = 9

print(longestConsecutiveSequence(i1) == o1)
print(longestConsecutiveSequence(i2) == o2)