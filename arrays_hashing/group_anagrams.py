def groupAnagrams(strs: list[str]) -> list[list[str]]:
    result = {}

    for s in strs:

        key = tuple(sorted(s))
        if key not in result:
            result[key] = [s]
        else:
            result[key].append(s)
    
    return result.values()


i1 = ["eat","tea","tan","ate","nat","bat"]
i2 = [""]
i3 = ["a"]

print(groupAnagrams(i1))
print(groupAnagrams(i2))
print(groupAnagrams(i3))