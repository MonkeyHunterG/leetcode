def productExceptSelf(nums: list[int]) -> list[int]:
    output = [1] * len(nums)
    prefix = [1] * len(nums)
    postfix = [1] * len(nums)

    # prefix
    for i in range(1, len(nums)):
        prefix[i] = nums[i - 1] * prefix[i - 1]
        output[i] = output[i] * prefix[i]
    # postfix
    for i in range(len(nums) - 2, -1, -1):
        postfix[i] = nums[i + 1] * postfix[i + 1]
        output[i] = output[i] * postfix[i]
    return output


input1 = [1,2,3,4]
output1 = [24, 12, 8, 6]

input2 = [-1, 1, 0, -3, 3]
output2 = [0, 0, 9, 0, 0]

print(productExceptSelf(input1))
print(productExceptSelf(input2))