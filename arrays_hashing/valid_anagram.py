def validAnagram(s: str, t: str) -> bool:
    if (len(s) != len(t)):
        return False
    s = sorted(s)
    t = sorted(t)
    for i in range(len(s)):
        if(s[i] != t[i]):
            return False
    return True


s1 = "anagram"
t1 = "nagaram"

s2 = "rat"
t2 = "car"

print(validAnagram(s1, t1))
print(validAnagram(s2, t2))