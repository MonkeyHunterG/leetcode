def topKFrequentElements(nums: list[int], k: int) -> list[int]:
    # k=count, v=[position]
    count = {}
    freq = [[] for i in range(len(nums)+1)]

    for i in range(len(nums)):
        if nums[i] not in count:
            count[nums[i]] = 1
        else:
            count[nums[i]] += 1
    print(count)

    for j, v in count.items():
        freq[v].append(j)
    print(freq)

    result = []
    for l in reversed(freq):
        for m in l:
            result.append(m)
            if len(result) == k:
                return result

    

nums1 = [1,1,1,1,2,2,3,4]
k1 = 2

nums2 = [1]
k2 = 1

print(topKFrequentElements(nums1, k1))
print(topKFrequentElements(nums2, k2))