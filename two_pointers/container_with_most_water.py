def container_with_most_water(height: list[int]) -> int:
    res = 0
    if len(height) < 2:
        return res

    l, r = 0, len(height) - 1
    length = len(height) - 1
    while l < r:
        vol = min(height[l], height[r]) * length
        if vol > res:
            res = vol
        if height[l] < height[r]:
            l += 1
        else:
            r -= 1
        length -= 1

    return res


i = [1, 8, 6, 2, 5, 4, 8, 3, 7]
o = 49
print(container_with_most_water(i) == o)
i = [1, 1]
o = 1
print(container_with_most_water(i) == o)
i = [1]
o = 0
print(container_with_most_water(i) == o)
