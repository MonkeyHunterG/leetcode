def three_sum(nums: list[int]) -> list[list[int]]:
    nums.sort()
    arr = []

    nums.sort()
    arr = []

    if len(nums) < 3:
        return arr

    dic = set()
    for i in range(len(nums) - 2):
        if nums[i] > 0:
            return arr

        if i > 0:
            while nums[i] == nums[i - 1]:
                i += 1
                if i == len(nums):
                    return arr

        l = i + 1
        r = len(nums) - 1

        while l < r:
            if nums[l] + nums[r] < -nums[i]:
                l += 1
                while nums[l] == nums[l - 1] and l < r:
                    l += 1
            elif nums[l] + nums[r] > -nums[i]:
                r -= 1
                while nums[r] == nums[r + 1]:
                    r -= 1
            else:
                tup = tuple([nums[i], nums[l], nums[r]])
                if tup not in dic:
                    dic.add(tup)
                    arr.append([nums[i], nums[l], nums[r]])
                l += 1

    return arr


i = [-1, 0, 1, 2, -1, -4]
o = [[-1, -1, 2], [-1, 0, 1]]
print(three_sum(i) == o)
i = [0, 1, 1]
o = []
print(three_sum(i) == o)
i = [0, 0, 0]
o = [[0, 0, 0]]
print(three_sum(i) == o)
i = [-4, -2, -2, -2, 0, 1, 2, 2, 2, 3, 3, 4, 4, 6, 6]
o = [[-4, -2, 6], [-4, 0, 4], [-4, 1, 3], [-4, 2, 2], [-2, -2, 4], [-2, 0, 2]]
print(three_sum(i) == o)
i = [0, 0, 0, 0]
o = [[0, 0, 0]]
print(three_sum(i) == o)
