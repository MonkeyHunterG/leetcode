def two_sum(numbers: list[int], target: int) -> list[int]:
    p1 = 0
    p2 = len(numbers) - 1

    while p1 < p2:
        if numbers[p1] + numbers[p2] == target:
            return [p1 + 1, p2 + 1]

        if numbers[p1] + numbers[p2] > target:
            p2 -= 1
        else:
            p1 += 1


i1 = ([2, 7, 11, 15], 9)
o1 = [1, 2]

i2 = ([2, 3, 4], 6)
o2 = [1, 3]

i3 = ([-1, 0], -1)
o3 = [1, 2]

i4 = ([5, 25, 75], 100)
o4 = [2, 3]

i5 = ([-1, -1, -1, -1, -1, -1, 1, 1], 2)
o5 = [7, 8]

i6 = ([-3, 3, 4, 90], 0)
o6 = [1, 2]

print(two_sum(i1[0], i1[1]) == o1)
print(two_sum(i2[0], i2[1]) == o2)
print(two_sum(i3[0], i3[1]) == o3)
print(two_sum(i4[0], i4[1]) == o4)
print(two_sum(i5[0], i5[1]) == o5)
print(two_sum(i6[0], i6[1]) == o6)
