def trap(height: list[int]) -> int:
    vol = 0
    if len(height) < 3:
        return vol

    l, r = 0, len(height) - 1
    h = 0

    while l < r:
        if height[l] < height[r]:
            if height[l] <= h:
                vol += h - height[l]
            else:
                h = height[l]
            l += 1
        else:
            if height[r] <= h:
                vol += h - height[r]
            else:
                h = height[r]
            r -= 1

    return vol


i = [4, 2, 0, 3, 2, 5]
o = 9
print(trap(i) == o)
i = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
o = 6
print(trap(i) == o)
