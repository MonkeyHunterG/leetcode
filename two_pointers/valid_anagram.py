def isPalindrome(s: str) -> bool:
    p1 = 0
    p2 = len(s) - 1
    while p1 < p2:
        while not s[p1].isalnum():
            p1 += 1
            if p1 > p2:
                return True
        while not s[p2].isalnum():
            p2 -= 1
        if s[p1].lower() != s[p2].lower():
            return False
        p1 += 1
        p2 -= 1
    return True


i1 = "A man, a plan, a canal: Panama"
o1 = True

i2 = "race a car"
o2 = False

i3 = " "
o3 = True

i4 = "aa"
o4 = True

print(isPalindrome(i1) == o1)
print(isPalindrome(i2) == o2)
print(isPalindrome(i3) == o3)
print(isPalindrome(i4) == o4)