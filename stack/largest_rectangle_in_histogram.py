def largest_rect(heights: list[int]) -> int:
    stack = []
    area = 0
    last_height = 0
    for j, height in enumerate(heights):
        start = j
        if height >= last_height:
            stack.append((j, height))
            last_height = height
            if height > area:
                area = height
        else:
            while len(stack) != 0 and stack[-1][1] > height:
                temp = (j - stack[-1][0]) * stack[-1][1]
                if temp > area:
                    area = temp
                index, h = stack.pop()
                start = index
            stack.append((start, height))

    for index in stack:
        temp = (len(heights) - index[0]) * index[1]
        if temp > area:
            area = temp

    return area


i = [2, 1, 5, 6, 2, 3]
o = 10
print(largest_rect(i) == o)
i = [2, 4]
o = 4
print(largest_rect(i) == o)
i = [3, 6, 5, 7, 4, 8, 1, 0]
o = 20
print(largest_rect(i) == o)
