def daily_temperatures(nums: list[int]) -> list[int]:
    res = [0] * len(nums)
    stack = []
    for n, t in enumerate(nums):
        while stack and t > stack[-1][0]:
            stack_temp, stack_ind = stack.pop()
            res[stack_ind] = n - stack_ind
        stack.append([t, n])
    return res


i = [73, 74, 75, 71, 69, 72, 76, 73]
o = [1, 1, 4, 2, 1, 1, 0, 0]
print(daily_temperatures(i) == o)
i = [30, 40, 50, 60]
o = [1, 1, 1, 0]
print(daily_temperatures(i) == o)
i = [30, 60, 90]
o = [1, 1, 0]
print(daily_temperatures(i) == o)
i = [3, 6, 4, 5]
o = [1, 0, 1, 0]
print(daily_temperatures(i) == o)
