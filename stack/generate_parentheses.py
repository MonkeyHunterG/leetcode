def generate_parentheses(n: int) -> list[str]:
    arr = []
    stack = []

    def backtrack(openN: int, closedN: int) -> str:
        if openN == closedN == n:
            arr.append("".join(stack))
            return
        if openN < n:
            stack.append("(")
            backtrack(openN + 1, closedN)
            stack.pop()
        if closedN < openN:
            stack.append(")")
            backtrack(openN, closedN + 1)
            stack.pop()

    backtrack(0, 0)

    return arr


i = 0
o = [""]
print(generate_parentheses(i) == o)
i = 1
o = ["()"]
print(generate_parentheses(i) == o)
i = 2
o = ["(())", "()()"]
print(generate_parentheses(i) == o)
i = 3
o = ["((()))", "(()())", "(())()", "()(())", "()()()"]
print(generate_parentheses(i) == o)
