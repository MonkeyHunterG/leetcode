def eval_rpn(tokens: list[str]) -> int:
    arr = []
    for s in tokens:
        match s:
            case "+":
                arr.append(arr.pop(-1) + arr.pop(-1))
            case "-":
                arr.append(arr.pop(-2) - arr.pop(-1))
            case "*":
                arr.append(arr.pop(-1) * arr.pop(-1))
            case "/":
                arr.append(int(arr.pop(-2) / arr.pop(-1)))
            case _:
                arr.append(int(s))
    return arr[0]


i = ["1", "2", "+", "3", "*", "4", "-"]
o = 5
print(eval_rpn(i) == o)
i = ["2", "1", "+", "3", "*"]
o = 9
print(eval_rpn(i) == o)
i = ["4", "13", "5", "/", "+"]
o = 6
print(eval_rpn(i) == o)
i = ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]
o = 22
print(eval_rpn(i) == o)
