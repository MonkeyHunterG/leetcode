def car_fleet(target: int, position: list[int], speed: list[int]) -> int:
    stack = []
    for i in range(len(position)):
        stack.append((position[i], speed[i]))

    if len(position) < 2:
        return len(position)

    stack = sorted(stack)

    time = (target - stack[len(stack) - 1][0]) / stack[len(stack) - 1][1]
    for j in range(len(stack) - 2, -1, -1):
        if (target - stack[j][0]) / stack[j][1] <= time:
            stack.pop(j)
        else:
            time = (target - stack[j][0]) / stack[j][1]

    return len(stack)


i1 = 10
i2 = [3]
i3 = [3]
o = 1
print(car_fleet(i1, i2, i3) == o)
# There is only one car, hence there is only one fleet.
i1 = 12
i2 = [10, 8, 0, 5, 3]
i3 = [2, 4, 1, 1, 3]
o = 3
print(car_fleet(i1, i2, i3) == o)
# The cars starting at 10 (speed 2) and 8 (speed 4) become a fleet, meeting each other at 12.
# The fleet forms at target.
# The car starting at 0 (speed 1) does not catch up to any other car, so it is a fleet by itself.
# The cars starting at 5 (speed 1) and 3 (speed 3) become a fleet, meeting each other at 6.
# The fleet moves at speed 1 until it reaches target.

i1 = 100
i2 = [0, 2, 4]
i3 = [4, 2, 1]
o = 1
print(car_fleet(i1, i2, i3) == o)
# The cars starting at 0 (speed 4) and 2 (speed 2) become a fleet, meeting each other at 4.
# The car starting at 4 (speed 1) travels to 5.
# Then, the fleet at 4 (speed 2) and the car at position 5 (speed 1) become one fleet, meeting each other at 6.
# The fleet moves at speed 1 until it reaches target.
