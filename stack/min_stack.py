class MinStack:
    arr = []
    min = []

    def __init__(self):
        self.arr = []
        self.min = []

    def push(self, val: int) -> None:
        if len(self.arr) > 0:
            if self.min[-1] >= val:
                self.min.append(val)
        else:
            self.min.append(val)
        self.arr.append(val)
        return None

    def pop(self) -> None:
        x = self.arr.pop(-1)
        if x == self.min[-1]:
            self.min.pop(-1)
        return None

    def top(self) -> int:
        return self.arr[-1]

    def get_min(self) -> int:
        return self.min[-1]


obj = MinStack()
obj.push(-2)
obj.push(0)
obj.push(-3)
print(obj.get_min() == -3)
obj.pop()
print(obj.top() == 0)
print(obj.get_min() == -2)
print("--------")

obj = MinStack()
obj.push(1)
print(obj.top() == 1)
print(obj.get_min() == 1)
print("--------")

obj = MinStack()
obj.push(0)
obj.push(1)
obj.push(0)
print(obj.get_min() == 0)
obj.pop()
print(obj.get_min() == 0)
