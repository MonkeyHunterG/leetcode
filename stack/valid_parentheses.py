def is_valid(s: str) -> bool:
    if len(s) % 2 != 0:
        return False
    p = 0
    b = 0
    c = 0

    arr = []

    for ch in s:
        match ch:
            case "(":
                p += 1
                arr.append(ch)
            case "[":
                b += 1
                arr.append(ch)
            case "{":
                c += 1
                arr.append(ch)

            case ")":
                if p < 1:
                    return False
                if arr[-1] != "(":
                    return False
                p -= 1
                arr.pop(-1)

            case "]":
                if b < 1:
                    return False
                if arr[-1] != "[":
                    return False
                b -= 1
                arr.pop(-1)

            case "}":
                if c < 1:
                    return False
                if arr[-1] != "{":
                    return False
                c -= 1
                arr.pop(-1)

    return len(arr) == 0


i = "()"
o = True
print(is_valid(i) == o)
i = "()[]{}"
o = True
print(is_valid(i) == o)
i = "(]"
o = False
print(is_valid(i) == o)
i = "([)]"
o = False
print(is_valid(i) == o)
i = "[([]])"
o = False
print(is_valid(i) == o)
i = "{[]}"
o = True
print(is_valid(i) == o)
i = "[{()}]"
o = True
print(is_valid(i) == o)
